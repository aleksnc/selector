"use strict";
const MAX__KEY = 3;
const LENG_CHILD = 3;
let mySkill = [];

let parent = '';

const SKILLS__TREE = {
    'jquery': ['jQuery', ['js', 'html']],
    'js': ['Java-Script', ['jquery', 'node', 'react', 'typescript', 'angular', 'bootstrap', 'opengl']],
    'react': ['React', ['node', 'typescript', 'webpack']],
    'typescript': ['TypeScript', ['angular']],
    'angular': ['Angular', ['node', 'typescript']],
    'node': ['Node Js', ['api', 'mongo', 'mysql', 'sql']],
    'html': ['HTML', ['css', 'js', 'grunt', 'gulp', 'react']],
    'pug': ['PUG', ['react']],
    'css': ['CSS', ['grunt', 'gulp', 'webpack', 'bootstrap']],
    'scss': ['SCSS', []],
    'less': ['Less', []],
    'bootstrap': ['bootstrap', []],
    'grunt': ['Grunt', ['bootstrap', 'scss', 'less', 'pug']],
    'gulp': ['Gulp', ['bootstrap', 'scss', 'less', 'pug']],
    'webpack': ['WebPack', ['bootstrap', 'scss', 'less']],
    'api': ['API', []],
    'php': ['PHP', ['api', 'mysql', 'laravel', 'modx', 'sql']],
    'laravel': ['Laravel', ['js', 'html', 'sql', 'mysql']],
    'modx': ['MODX', ['js', 'html', 'sql', 'mysql']],
    'joomla': ['Joomla', ['js', 'html', 'sql', 'mysql']],
    'python': ['Python', ['api', 'mysql', 'mongo', 'sql', 'post']],
    'sql': ['SQL', ['mysql', 'sqls']],
    'mysql': ['MySQL', []],
    'sqls': ['SQL Server', []],
    'mongo': ['Mongo DB', []],
    'post': ['Postgress', []],
    'opengl': ['OpenGL', []],
    'cplusplus': ['C++', ['opengl']],
    'csharp': ['C#', ['opengl', 'asp']],
    'asp': ['ASP.NET', ['sqls']]
}

const GROUP__SKILLS = [
    ["Быстрый создатель миражей", ['pug', 'scss', 'grunt']],
    ["Великий пыхарь", ['php', 'mysql', 'laravel']],
    ["Пыхарь", ['php', 'mysql', 'php']],
    ["Укротитель змей!", ['python', 'mongo', 'api']],
    ["Реактивный содатель миражей", ['php', 'mysql', 'php']],
    ["Стремный треугольник", ['angular', 'typescript', 'api']],
    ["Создатель миражей", ['html', 'css', 'jquery']],
]



const colorStep = ['#e84830', '#e09a13', '#dedc02', '#afdf31', '#6fa93c', '#00efff', '#00a6ff'];

function diff(a1, a2) {
    return a1.filter(i=>!a2.includes(i))
        .concat(a2.filter(i=>!a1.includes(i))).length===0
}

function skill(key, value) {
    let newSkill = $('.skill.parent').clone().removeClass('parent');

    newSkill.find('.skill__name').empty().html(value[0])

    newSkill.data("key", key);
    newSkill.data("color", null);
    $('.space').append(newSkill);
}

function skillChild(key, step) {
    if (SKILLS__TREE[key][1] == undefined) {
        return;
    }
    let listChild = SKILLS__TREE[key][1];

    listChild.map((item) => {
        let childStep = step;
        let skill = $("div:data(key==" + item + ")");
        let skillData = skill.data('color');

        if (skillData !== null) {
            // console.log(skillData, step);
            if (skillData > step) {
                skill.not('.select').addClass('selectChild').css({
                    'border-color': colorStep[step],
                    'background-color': colorStep[step],
                    'color': '#fff'
                });
            }
        } else {
            //  console.log(skillData, step);
            skill.data('color', step);
            skill.not('.select').addClass('selectChild').css({
                'border-color': colorStep[step],
                'background-color': colorStep[step],
                'color': '#fff'
            });
        }
        // console.log('parent:',parent ,' item: ',item,' step: ',step);
        if (childStep < LENG_CHILD && parent !== item) {
            childStep++;
            skillChild(item, childStep);
        }
    });

    parent = key;

}

function selectDisable() {
    $('.skill').not('.select, .selectChild').css('border-color', 'gray');
}

$(document).ready(function () {
    let maxKey =MAX__KEY;
    $.each(SKILLS__TREE, function (key, value) {
        skill(key, value);
    })

    $('.skill').click(function () {
        if(maxKey<MAX__KEY&&$(this).data('color')===null){
            let itsOk = confirm("Этот скилл никак не свячзан с выбранным(и). Вы уверены что хотите выбрать именно его?");
            if(!itsOk){
                return;
            }
        }
        $('.next').fadeIn();
        if (maxKey > 0) {
            if (!$(this).hasClass('select')) {
                maxKey--;
            } else {
                return;
            }

            $(this).addClass('select');
            $(this).data('color', 0).css({
                'border-color': colorStep[0],
                'background-color': colorStep[0],
                'color': '#fff'
            });
            let key = $(this).data('key');
            parent = key;
            mySkill.push(key);
            skillChild(key, 1);
            if (MAX__KEY <= 0) {
                selectDisable()
            }
        } else {
            selectDisable()
        }
    });

    $('.next').click(function () {
       // $('.space').hide();
        $('.next').hide();
        // $('.user').show();
        // let race = "ХЗ кто o_O..."
        // let type ="";
        //
        // GROUP__SKILLS.map((item, index) => {
        //     console.log(GROUP__SKILLS[index][1], mySkill);
        //     console.log(diff(GROUP__SKILLS[index][1], mySkill));
        //
        //     if(diff(GROUP__SKILLS[index][1], mySkill)) {
        //         race=GROUP__SKILLS[index][0];
        //         type =index;
        //     }
        // });
        //
        // $('.user__race .race').empty().html(race);
        // $('.user__img').addClass('class'+type);

    })

});
